import React from 'react';
import MainRouter from './routers/MainRouter';
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useTranslation } from 'react-i18next';

const BtnColorContext = React.createContext('btn btn-darkyellow')

function App() {
  const { t, i18n } = useTranslation();
  const lng = localStorage.getItem('language');
  i18n.changeLanguage(lng)
  return (
    <div >
      <BtnColorContext.Provider value="btn btn-info">
        <MainRouter />
      </BtnColorContext.Provider>
    </div>
  )
}

export default App;