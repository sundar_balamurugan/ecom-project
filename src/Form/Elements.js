import React from 'react';
import { InputGroup, FormControl, Alert } from 'react-bootstrap';

function Elements(props) {
    const fieldconfig = props.field;
    console.log(props, ' ', props.state,)
    const error = props.state.error;
    let element = null;
    switch (fieldconfig.type) {
        case 'input':
            console.log(fieldconfig);
            element = (
                <div>
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">{fieldconfig.id}</InputGroup.Text>
                        <FormControl
                            name={fieldconfig.name}
                            type={fieldconfig.type}
                            placeholder={fieldconfig.placeholder}
                            onChange={props.changed}
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                        />
                    </InputGroup>
                    {fieldconfig.id == 'name' && error.nameerror &&
                        <Alert variant='danger' className='span'>
                            {error.nameerror}
                        </Alert>
                    }
                    {fieldconfig.id == 'address' && error.addresserror &&
                        <Alert variant='danger' className='span'>
                            {error.addresserror}
                        </Alert>
                    }
                    {fieldconfig.id == 'email' && error.emailerror &&
                        <Alert variant='danger' className='span'>
                            {error.emailerror}
                        </Alert>
                    }
                </div>)
            break;
        case 'select':
            element = (
                <div>
                    <select
                        className="mb-3"
                        name={fieldconfig.id}
                        onChange={props.changed}
                    >
                        <option  >{fieldconfig.placeholder}</option>
                        {fieldconfig.options.map((state, index) =>
                            <option key={index} value={state.value} >
                                {state.value}
                            </option>)
                        }
                    </select>
                    {error.stateerror &&
                        <Alert variant='danger' className='span'>
                            {error.stateerror}
                        </Alert>
                    }
                </div>
            )
            break;

        case 'checkbox':
            element = (
                <div>
                    {fieldconfig.id}
                    {fieldconfig.label.map((check, id) =>
                        <div key={id} >
                            <input type={fieldconfig.type} />
                            <label>{check.value}</label>
                        </div>
                    )}
                    {error.gendererror &&
                        <Alert variant='danger' className='span'>
                            {error.gendererror}
                        </Alert>
                    }
                </div>
            )
            break;

        case 'file':
            element = (
                <div>
                    <input type={fieldconfig.type} />
                    {error.fileerror &&
                        <Alert variant='danger' className='span'>
                            {error.fileerror}
                        </Alert>
                    }
                </div>
            )
            break;
        default:
            console.log('exit')
    }
    return element;
}

export default Elements;
