export const Fields = [
    {
        "id": "name",
        "placeholder": "Enter name",
        "type": "input",
        "value": "",
        "valid": false,
        "validation": {
            "required": true,
            "pattern": /^[a-z]+$/i
        }
    },
    {
        "id": "email",
        "placeholder": "Enter email",
        "type": "input",
        "value": "",
        "valid": false,
        "validation": {
            "required": true,
            "IsEmail": true
        }
    },
    {
        "id": "gender",
        "type": "checkbox",
        "label": [
            {
                "value": "male"
            },
            {
                "value": "female"
            }
        ],
        "validation": {
            "required": true
        }
    },
    {
        "id": "state",
        "placeholder": "select state",
        "type": "select",
        "options": [
            {
                "value": "TamilNadu"
            },
            {
                "value": "Karnataka"
            }
        ],
        "value": "",
        "valid": false,
        "validation": {
            "required": true
        }
    },
    {
        "id": "address",
        "placeholder": "Enter Address",
        "type": "input",
        "value": "",
        "valid": false,
        "validation": {
            "required": true,
            "pattern": /.{3}/
        }
    },
    {
        "id": "file",
        "type": "file",
        "validation": {
            "required": true
        }
    }
]
