import React from 'react';
import { Fields } from './Formjson';
import Elements from './Elements'
import { Container } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

class SampleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formvalid: false,
            error: {
                nameerror: ' ',
                addresserror: ' ',
                emailerror: ' ',
                stateerror: ' ',
                gendererror: ' ',
                fileerror: ' '
            }
        }
    }

    handleChange = (e, field, id) => {
        const updatefield = { ...field };
        updatefield.value = e.target.value;
        const isvalid = this.validation(updatefield, this.state.error);
        this.setState({ error: isvalid.error })
        const valid = isvalid.error.nameerror || isvalid.error.addresserror ||
            isvalid.error.emailerror || isvalid.error.gendererror || isvalid.error.fileerror
        if (this.state.error.nameerror || this.state.error.addresserror ||
            this.state.error.emailerror || this.state.error.gendererror ||
            this.state.error.fileerror) {
            this.setState({
                formvalid: true
            })
        }
    }

    validation = (field, errors) => {
        const rules = field.validation;
        const value = field.value;
        let result = {};
        if (!rules) {
            return true;
        }
        console.log(errors, 'errors')
        switch (field.id) {
            case 'name':
                result = {
                    error: rules.pattern.test(value)
                        ? { ...errors, nameerror: '' }
                        : { ...errors, nameerror: 'Invalid' }
                }
                console.log(result, 'errors')
                break;

            case 'address':
                result = {
                    error: rules.pattern.test(value)
                        ? { ...errors, addresserror: '' }
                        : { ...errors, addresserror: 'Invalid' }
                }
                break;

            case 'email':
                result = {
                    error: rules.required
                        ? { ...errors, emailerror: '' }
                        : { ...errors, emailerror: 'Invalid' }
                }
                break;

            case 'state':
                result = {
                    error: rules.required
                        ? { ...errors, stateerror: '' }
                        : { ...errors, stateerror: 'Invalid' }
                }
                break;
            case 'gender':
                result = {
                    error: rules.required
                        ? { ...errors, gendererror: '' }
                        : { ...errors, gendererror: 'Invalid' }
                }
                break;
            case 'file':
                result = {
                    error: rules.required
                        ? { ...errors, fileerror: '' }
                        : { ...errors, fileerror: 'Invalid' }
                }
                break;
            default: console.log('exit1')
        }
        return result
    }

    render() {
        const data = Fields;
        console.log(this.state.formvalid)
        return (
            <form>
                <Container className="sampleform">
                    <h3 id="heading">Form</h3>
                    {data.map((field, id) =>
                        <Elements
                            key={field.id}
                            state={this.state}
                            field={field}
                            changed={(e) => { this.handleChange(e, field, id) }}
                        />
                    )}
                    <Button disabled={!this.state.formvalid}>
                        submit
                    </Button>
                </Container>
            </form>
        )
    }
}

export default SampleForm;