import { getProducts, fetchcartProduct } from '../actions/productsActions';
import { GET_PRODUCTS, FETCH_CARTPRODUCT } from '../actions/actionTypes';

test('GetProducts', () => {
    const result = getProducts([{ id: '1' }])
    expect(result.type).toBe(GET_PRODUCTS);
});

test('fetchCartProduct', () => {
    const result = fetchcartProduct([])
    expect(result.type).toBe(FETCH_CARTPRODUCT)
})