import React from 'react';
import axios from './mock/axios';
import { render, waitFor } from '@testing-library/react';
import { getAllProducts } from '../actions/productsActions';

describe(' axios API Call mock testing', () => {
    test('Component rendered successfully', async () => {
        axios.get.mockImplementationOnce(() => Promise.resolve({
            status: 200,
            data: {
                brand: "Dell"
            }
        }))
        const data = await getAllProducts()
        //console.log(data);

        expect(data.data).toEqual([
            {
                "id": "1",
                "name": "Laptop",
                "price": "15000",
                "brand": "Acer",
                "category": "Laptops",
                "desc": "acer NITRO 5 Ryzen 5 Hexa Core 5600H - (8 GB/1 TB HDD/256 GB SSD/Windows 10 Home/4 GB Graphics/NVIDIA GeForce GTX 1650/144 Hz) AN515-45-R712 Gaming Laptop",
                "ProductImage": "https://i.pinimg.com/originals/ed/3d/c8/ed3dc871965cae3a95c619bc69acece5.jpg",
                "cart": false
            },
            {
                "id": "2",
                "name": "Laptop",
                "price": "20000",
                "brand": "Dell",
                "category": "Laptops",
                "desc": "DELL G5 15 SE Ryzen 5 Hexa Core 4600H - (8 GB/512 GB SSD/Windows 10 Home/6 GB Graphics/AMD Radeon RX 5600M/120 Hz) G5 5505 Gaming Laptop",
                "ProductImage": "https://i.pinimg.com/originals/8a/8e/83/8a8e83197ff19458703bf8c9b8619f53.jpg",
                "cart": true,
                "Cart": false
            },
            {
                "id": "3",
                "name": "Laptop",
                "price": "17000",
                "brand": "Lenovo",
                "category": "Laptops",
                "desc": "Lenovo Ideapad S145 Ryzen 3 Dual Core 3200U - (4 GB/1 TB HDD/Windows 10 Home) S145-15API Laptop",
                "ProductImage": "https://pcsupport.lenovo.com/bo/en/products/laptops-and-netbooks/lenovo-e-series-laptops/lenovo-e49-notebook/3464/34641b1/solutions/~/media/Images/ContentImages/Consumer%20Figures/pub_image/l/lenovo_e49_20120814.ashx?w=300&h=300",
                "cart": true
            },
            {
                "id": "4",
                "name": "Mobile",
                "price": "12000",
                "brand": "Redmi",
                "category": "Mobiles",
                "desc": "6 GB RAM | 64 GB ROM | Expandable Upto 512 GB 16.94 cm (6.67 inch) Full HD+ Display 5000 mAh Lithium-ion Polymer Battery",
                "ProductImage": "https://e-spotechnologies.com/wp-content/uploads/2020/06/REDMI-NOTE-9-3GB64GB-1-1-300x300.jpg",
                "cart": false
            },
            {
                "id": "5",
                "name": "Mobile",
                "price": "18000",
                "brand": "Oneplus",
                "category": "Mobiles",
                "desc": "48MP+8MP+5MP+2MP quad rear camera with 1080P Video at 30/60 fps, 4k 30fps | 32MP+8MP front dual camera with 4K video capture at 30/60 fps and 1080 video capture at 30/60 fps",
                "ProductImage": "https://www.reliancedigital.in/medias/OnePlus-Nord-Gray-Onyx-8-128-491894432-i-1-1200Wx1200H-300Wx300H?context=bWFzdGVyfGltYWdlc3w3NjAyNXxpbWFnZS9qcGVnfGltYWdlcy9oODIvaGI4LzkzMzcyMTQ4OTQxMTAuanBnfGNiNDVmYzUwODgxZmE2ZTk5Nzc4ZWRlZDJiNGU5YTRkYmI5NDdlNjAzODAxYmVjMDJmOGRiODdiY2RlZDMxZTY",
                "cart": false
            },
            {
                "id": "6",
                "name": "Laptop",
                "price": "16000",
                "brand": "HCL",
                "category": "Laptops",
                "desc": "HCL Ideapad S145 Ryzen 3 Dual Core 3200U - (4 GB/1 TB HDD/Windows 10 Home) S145-15API Laptop",
                "ProductImage": "https://www.computerbazaronline.com/image/cache/catalog/product/hcl-300x300.jpg",
                "cart": false
            },
            {
                "id": "7",
                "name": "Laptop",
                "price": "19000",
                "brand": "HP",
                "category": "Laptops",
                "desc": "HP Ideapad S145 Ryzen 3 Dual Core 3200U - (4 GB/1 TB HDD/Windows 10 Home) S145-15API Laptop",
                "ProductImage": "https://bookshop.binrush.com/wp-content/uploads/2020/07/HP-250-G6-notebook-second-300x300.jpg",
                "cart": false
            },
            {
                "id": "8",
                "name": "Mobile",
                "price": "14000",
                "brand": "Samsung",
                "category": "Mobiles",
                "desc": "48MP+8MP+5MP+2MP quad rear camera with 1080P Video at 30/60 fps, 4k 30fps | 32MP+8MP front dual camera with 4K video capture at 30/60 fps and 1080 video capture at 30/60 fps",
                "ProductImage": "https://cdn.sharafdg.com/cdn-cgi/image/width=300,height=300,fit=pad/assets/f/0/f/9/f0f9160ca86fdb781d9be8ec189050c21f5de2f4_000000000001219906_1.jpg",
                "cart": false
            },
            {
                "id": "9",
                "name": "Mobile",
                "price": "12000",
                "brand": "Huawei",
                "category": "Mobiles",
                "desc": "48MP+8MP+5MP+2MP quad rear camera with 1080P Video at 30/60 fps, 4k 30fps | 32MP+8MP front dual camera with 4K video capture at 30/60 fps and 1080 video capture at 30/60 fps",
                "ProductImage": "https://cdn.abplive.com/onecms/images/product/25e723a441e64f3de724de14b1840f5e.jpg?impolicy=abp_cdn&imwidth=300",
                "cart": false
            },
            {
                "id": "10",
                "name": "Mobile",
                "price": "11000",
                "brand": "Itel",
                "category": "Mobiles",
                "desc": "6 GB RAM | 64 GB ROM | Expandable Upto 512 GB 16.94 cm (6.67 inch) Full HD+ Display 5000 mAh Lithium-ion Polymer Battery",
                "ProductImage": "https://www.mobilesjin.com/wp-content/uploads/2020/10/itel-vision-1-plus-price-in-pakistan.jpg",
                "cart": false
            },
            {
                "id": "11",
                "name": "Mobile",
                "price": "14000",
                "brand": "Lenovo",
                "category": "Mobiles",
                "desc": "6 GB RAM | 64 GB ROM | Expandable Upto 512 GB 16.94 cm (6.67 inch) Full HD+ Display 5000 mAh Lithium-ion Polymer Battery",
                "ProductImage": "https://eg.jumia.is/unsafe/fit-in/300x300/filters:fill(white)/product/06/981661/1.jpg?3087",
                "cart": false
            },
            {
                "id": "12",
                "name": "Mobile",
                "price": "14000",
                "brand": "Samsung",
                "category": "Mobiles",
                "desc": "6 GB RAM | 64 GB ROM | Expandable Upto 512 GB 16.94 cm (6.67 inch) Full HD+ Display 5000 mAh Lithium-ion Polymer Battery",
                "ProductImage": "https://e-spotechnologies.com/wp-content/uploads/2020/06/REDMI-NOTE-9-3GB64GB-1-1-300x300.jpg",
                "cart": false
            }
        ]);
    })
})


