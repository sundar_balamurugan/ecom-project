import React from 'react';
import { Provider } from 'react-redux';
import Home from '../containers/Home';
import { render, screen } from '@testing-library/react';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import userReducer from '../reducers/userReducer';
import '@testing-library/jest-dom'

const store = createStore(userReducer, applyMiddleware(thunk));

test('HomeContainer', () => {
    render(
        <Provider store={store}>
            <Home />
        </Provider>
    );
    const temp = screen.getByAltText('Loader');
    expect(temp).toBeInTheDocument();
})