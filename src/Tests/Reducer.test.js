import userReducer from '../reducers/userReducer';
import { CREATE_USER } from '../actions/actionTypes';

test('Adding User', () => {
    const user = {
        "firstname": "sundar",
        "lastname": "bala",
        "mobile": "8489876537",
        "password": "radnus1234"
    };
    expect(userReducer(undefined, {
        type: CREATE_USER,
        user
    })).toEqual({
        user
    });
});