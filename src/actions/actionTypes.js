export const STORE_USER = 'STORE_USER';

export const CHANGE_USER = 'CHANGE_USER';

export const LOG_OUT = 'LOG_OUT';

export const PRODUCT_DETAILS = 'PRODUCT_DETAILS';

export const GET_PRODUCTS = 'GET_PRODUCTS';

export const FETCH_CARTPRODUCT = 'FETCH_CARTPRODUCT';

export const CREATE_USER = 'CREATE_USER';