import axios from 'axios';
import {
    GET_PRODUCTS,
    FETCH_CARTPRODUCT
} from './actionTypes';

export function getProducts(products) {
    return { type: GET_PRODUCTS, products };
}

export function getAllProducts() {
    return axios.get('https://60cad89c21337e0017e43279.mockapi.io/products');
}

export function getCartProducts() {
    return (dispatch) => {
        axios.get('https://60cad89c21337e0017e43279.mockapi.io/cart')
            .then((res) => {
                dispatch(fetchcartProduct(res.data));
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export const fetchcartProduct = (product) => {
    return {
        type: FETCH_CARTPRODUCT,
        payload: product
    }
}

export function cartProduct(product) {
    return (dispatch) => {
        axios.post(`https://60cad89c21337e0017e43279.mockapi.io/cart`, product)
            .then(res => {
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export function RemovecartProduct(id, product) {
    return (dispatch) => {
        axios.delete(`https://60cad89c21337e0017e43279.mockapi.io/cart/${id}`, product)
            .then((res) => { })
            .catch((err) => {
                console.log(err);
            })
    }
}

export function addOrder(user, orders) {
    user.orders = [...user.orders, ...orders];
    return (dispatch) => {
        axios.put(`https://60cad89c21337e0017e43279.mockapi.io/user/${user.id}`, user)
            .then(res => { })
            .catch((err) => {
                console.log(err);
            })
    }
}