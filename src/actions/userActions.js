import { CHANGE_USER, LOG_OUT } from './actionTypes';
import axios from 'axios';
import History from '../routers/History';

export function createUser(data) {
    const user = {
        firstname: data.firstname,
        lastname: data.lastname,
        mobile: data.mobile,
        password: data.password,
        addressline1: data.addressline1,
        addressline2: data.addressline2,
        state: data.state,
        city: data.city,
        pincode: data.pincode,

    };

    return (dispatch) => {
        axios.post(`https://60cad89c21337e0017e43279.mockapi.io/user`, user)
            .then(res => { })
            .catch((err) => {
                console.log(err);
            })
    };
}

export function fetchUser(user) {
    return (dispatch) => {
        axios.get(`https://60cad89c21337e0017e43279.mockapi.io/user`)
            .then((res) => {
                res.data.map(data => {
                    if (data.mobile === user.mobile && data.password === user.password) {
                        dispatch(changeUser(data.id))
                        History.push('/');
                    }
                    else {
                        const flag = true;
                        History.push('/login', { state: flag });
                    }
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export function changeUser(id) {
    return (dispatch) => {
        axios.get(`https://60cad89c21337e0017e43279.mockapi.io/user/${id}`)
            .then((res) => {
                window.localStorage.setItem('id', res.data.id);
                dispatch(loguser(res.data))
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export function loguser(user) {
    return {
        type: CHANGE_USER,
        user
    }
}

export function logOutUser() {
    return {
        type: LOG_OUT
    }
}
