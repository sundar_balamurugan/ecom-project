import React from 'react';
import { Row, Col, Carousel, Container } from 'react-bootstrap';
import { withNamespaces } from 'react-i18next';
import i18n from '../i18n';
import { useTranslation } from 'react-i18next';

function Banner(props) {
    const { t, i18n } = useTranslation()
    return (
        <Container className="banner">

            <Carousel variant="dark">
                <Carousel.Item interval={1000}>
                    <img
                        className="d-block w-100"
                        id="viewimage1"
                        src="https://i.pinimg.com/originals/ed/3d/c8/ed3dc871965cae3a95c619bc69acece5.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>Lenovo Laptop</h3>
                        <p>{t('get Up To 20% off..')}</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item interval={500}>
                    <img
                        className="d-block w-100"
                        id="viewimage1"
                        src="https://www.mobilesjin.com/wp-content/uploads/2020/10/itel-vision-1-plus-price-in-pakistan.jpg"
                        alt="Second slide"
                    />
                    <Carousel.Caption>
                        <h3>Asus Mobile</h3>
                        <p>{t('get Up To 50% off..')}</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        id="viewimage1"
                        src="https://eg.jumia.is/unsafe/fit-in/300x300/filters:fill(white)/product/06/981661/1.jpg?3087"
                        alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h3>Lenovo Mobiles</h3>
                        <p>{t('get Up To 50% off..')}</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </Container>
    )
}

export default Banner;