import { Row, Container, Button, Alert } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function DetailComponent(props) {

    const { product } = props.state;
    const { t } = useTranslation();
    return (
        <Container>
            <Row id="viewtable">
                {props.state.cartsuccess &&
                    <Alert variant='warning' >
                        <center> {t('Product added To Cart')} </center>
                    </Alert>
                }
                <h2 id='productsheader'>
                    {product.brand + ' ' + product.name}
                </h2>
                <img
                    id='viewimage'
                    src={product.ProductImage}
                    alt={product.name}
                />
                <div id='productdetail'>
                    <p>
                        <strong> {t('Product Name')} </strong> :
                        {product.name}
                    </p>
                    <p>
                        <strong>{t('Price')} </strong> : Rs.
                        {product.price}/-
                    </p>
                    <p> <strong>{t('Brand')} </strong> :
                        {product.brand}
                    </p>
                    <p>
                        <strong>{t('Category')} </strong> :
                        {product.category}
                    </p>
                    <p>
                        <strong>{t('Description')} </strong> :
                        {product.desc}.
                    </p>
                </div>
                <p id='viewbutton' >
                    <Button
                        variant='primary'
                        className='viewbuttons'>
                        {t('Buy now')}
                    </Button>
                    <Button
                        variant='warning'
                        className='viewbuttons'
                        onClick={() => { props.cart(product) }}
                    >
                        {t('Add to Cart')}
                    </Button>
                </p>
            </Row>
        </Container >
    )
}

export default DetailComponent;