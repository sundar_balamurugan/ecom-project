import React from 'react';

export const myHoc = Wrapped => props => {
    const data = 'Shopping';
    return (
        <Wrapped {...props} data={data} />
    )
}

