import { MOBILE, PASSWORD } from '../utils/constants/inputNames';
import { Alert, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function LoginForm(props) {
    const { t } = useTranslation();
    return (
        <form >
            <div id='userForm'>
                <p id='formtitle'> {t('User Login')} </p>
                {props.state.loginError &&
                    <Alert variant='danger' className='span'>
                        {t('Invalid Username or Password')}
                    </Alert>
                }
                <p className='label'>{t('Mobile')} </p>
                <input
                    name={MOBILE}
                    value={props.state.mobile}
                    onChange={(e) => props.change(e)}
                    className='forminput'
                    placeholder={t('Enter Mobile Number')} />
                {props.state.error.mobileError &&
                    <Alert variant='danger' className='span'>
                        {props.state.error.mobileError}
                    </Alert>
                }

                <p className='label'>{t('Password')} </p>
                <input
                    type='password'
                    name={PASSWORD}
                    value={props.state.password}
                    onChange={(e) => props.change(e)}
                    className='forminput'
                    placeholder={t('Enter Password')} />
                {props.state.error.passwordError &&
                    <Alert variant='danger' className='span'>
                        {props.state.error.passwordError}
                    </Alert>
                }

                <Button
                    type='Submit'
                    id='formbutton'
                    onClick={(e) => props.submit(e)}
                    disabled={props.state.error.mobileError
                        || props.state.error.passwordError} >
                    {t('Login')}
                </Button>

                <p id='createaccount'>
                    <a href='/create-account'>{t('Create New Account')}</a>
                </p>

            </div>
        </form>
    )
}

export default LoginForm;