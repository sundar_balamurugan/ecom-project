import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { MDBIcon } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { bindActionCreators } from 'redux';
import { logOutUser } from '../actions/userActions';
import { NavDropdown, Container, Navbar, Nav, Button, Form } from 'react-bootstrap';
import History from '../routers/History';
import { withTranslation } from 'react-i18next';

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleaccount = () => {
        History.push(`/${this.props.match.params.lng}/my-account`);
    }

    handlecart = () => {
        History.push(`/${this.props.match.params.lng}/cart`)
    }

    handleYourOrder = (id) => {
        History.push(`/${this.props.match.params.lng}/your-order/${id}`)
    }

    handlecategory = (category) => {
        console.log(category);
        History.push(`/${this.props.match.params.lng}/products`, { state: category })
    }

    handleLogout = () => {
        this.props.logOutUser();
        History.push(`/${this.props.match.params.lng}/`);
    }

    render() {
        const { t, i18n } = this.props;
        const flag = JSON.parse(window.localStorage.getItem('isLogin'))
        const { cartproduct } = this.props;
        return (
            <>
                <div expand="lg" expanded="true" className="mainbar">
                    <Navbar variant="light" bg="light" expand="md" >
                        <Container>
                            <Navbar.Brand >{t('E KART')}</Navbar.Brand>
                        </Container>
                        {flag ?
                            <>
                                <MDBIcon icon="cart-plus"
                                    onClick={() => { this.handlecart() }} />
                                <sup id="sup" >
                                    {cartproduct && cartproduct.length}</sup>
                                <NavDropdown title={`Hi ${this.props.loguser && this.props.loguser.firstname}`}
                                    id="navbarScrollingDropdown">
                                    <NavDropdown.Item onClick={() => this.handleaccount()}>
                                        {t('My Account')}</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handleYourOrder(this.props.loguser.id)}>
                                        {t('Your order')}</NavDropdown.Item>
                                </NavDropdown>
                                <Form className="d-flex">
                                    <Button variant="outline-success"
                                        onClick={() => this.handleLogout()}
                                    >
                                        {t('Logout')}
                                    </Button>
                                </Form>
                            </> :
                            <>
                                <Link className="link1" to='/login'>{t('Login')}</Link>
                            </>
                        }
                    </Navbar>
                </div>
                <div className="mainbar1">
                    <Navbar bg="dark" variant="dark" expand="sm" >
                        <Container >
                            <Navbar.Brand >{t('Category')}</Navbar.Brand>
                            <Nav className="me-auto"  >
                                <Nav.Link onClick={(v) => this.handlecategory('Laptops')}>Laptop</Nav.Link>
                                <Nav.Link onClick={(v) => this.handlecategory('Mobiles')}>Mobiles</Nav.Link>
                            </Nav>
                        </Container>
                    </Navbar>
                </div>
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        cartproduct: state.cartproduct,
        loguser: state.loggedUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ logOutUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Navigation));