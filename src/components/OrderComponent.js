import { Container, Table, Alert, Button } from 'react-bootstrap';
import {
    CARD_NUMBER,
    CASH_ON_DELIVERY,
    CREDIT_CARD,
    CVV,
    DEBIT_CARD
} from '../utils/constants/inputNames';
import { useTranslation } from 'react-i18next';

function OrderComponent(props) {
    const { products } = props.state;
    const user = props.user;
    const { t } = useTranslation();
    return (
        <Container>
            {props.state.success &&
                <div id="success">
                    <p id="success-message">{t('Your Order is Confirmed !')}</p>
                    <Button
                        variant='success'
                        id='success-button2'
                        onClick={() => props.redirect('home')}>
                        {t('Home')}
                    </Button>
                </div>
            }
            <Table className='orderdetail'>
                <tbody>
                    <tr>
                        <td colSpan='2'>
                            <h2 id='productsheader'> {t('Product Details')} </h2>
                        </td>
                    </tr>
                    {products.map((product) =>
                        <tr key={product.id} >
                            <td>
                                <img
                                    src={product.ProductImage}
                                    alt={product.name} />
                            </td>
                            <td>
                                <div className='productdetails'>
                                    <p>
                                        <strong> {t('Product Name')} </strong> :
                                        {product.name}
                                    </p>
                                    <p>
                                        <strong>{t('Price')} </strong> : Rs.
                                        {product.price}/-
                                    </p>
                                    <p>
                                        <strong>{t('Brand')} </strong> :
                                        {product.brand}
                                    </p>
                                    <p>
                                        <strong>{t('Description')} </strong> :
                                        {product.desc}.
                                    </p>
                                    <div>
                                        <p id='count-label'>
                                            <strong>{t('No. of Items')} </strong>   :
                                        </p>
                                        <div id='count-buttons' >
                                            <button
                                                className='count'
                                                onClick={() => props.count('decrement', product)}>
                                                -
                                            </button>
                                            <p id='count-value'>
                                                {props.state.count}
                                            </p>
                                            <button
                                                className='count'
                                                onClick={() => props.count('increment', product)}>
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>)
                    }
                </tbody>
            </Table>
            <Table className='orderdetail'>
                <tbody>
                    <tr>
                        <td>
                            <h2 id='productsheader'> {t('User Details')} </h2>
                        </td>
                        <td>
                            <h2 id='productsheader'> {t('Payment Options')} </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className='paymentdetails'>
                                <p>
                                    <strong> {t('UserName')} </strong> :
                                    <br />
                                    {`${user.firstname} ${user.lastname}`}
                                </p>
                                <p>
                                    <strong> {t('Address')} </strong>  :
                                    <br />
                                    {`${user.firstname} ${user.lastname},`}
                                    <br />
                                    {`${user.addressline1} ${user.addressline2}`}
                                    <br />
                                    {`${user.city}, `}
                                    <br />
                                    {`${user.state} - ${user.pincode}. `}
                                </p>
                                <p>
                                    <strong> {t('Mobile')} </strong> :
                                    <br />
                                    {user.mobile}
                                </p>
                            </div>
                        </td>
                        <td>
                            <div className='paymentdetails'>
                                <p>
                                    <strong> {t('Choose Payment Type')} </strong> :
                                </p>
                                <p>
                                    <input
                                        type='radio'
                                        name='payment'
                                        value={CREDIT_CARD}
                                        onChange={(e) => props.payment(e)} />
                                    {t('Credit Card')}
                                </p>
                                {props.state.creditCardDetail &&
                                    <div className='cardinfo' >
                                        <p className='cardlabel'>{t('Card Number')} </p>
                                        <input
                                            name={CARD_NUMBER}
                                            className='carddetail'
                                            placeholder='Enter Card Number'
                                            onChange={(e) => props.change(e)} />
                                        {props.state.error.cardnumberError &&
                                            <Alert
                                                variant='danger'
                                                className='card' >
                                                {props.state.error.cardnumberError}
                                            </Alert>
                                        }
                                        <p className='cardlabel'>CVV</p>
                                        <input
                                            className='cvv'
                                            name={CVV}
                                            placeholder='Enter CVV'
                                            onChange={(e) => props.change(e)} />
                                        {props.state.error.cvvError &&
                                            <Alert
                                                variant='danger'
                                                className='card'>
                                                {props.state.error.cvvError}
                                            </Alert>
                                        }
                                    </div>
                                }
                                <p>
                                    <input
                                        type='radio'
                                        name='payment'
                                        value={DEBIT_CARD}
                                        onChange={(e) => props.payment(e)} />
                                    {t('Debit Card')}
                                </p>
                                {props.state.debitCardDetail &&
                                    <div className='cardinfo' >
                                        <p className='cardlabel'>{t('Card Number')} </p>
                                        <input
                                            name={CARD_NUMBER}
                                            className='carddetail'
                                            placeholder='Enter Card Number'
                                            onChange={(e) => props.change(e)} />
                                        {props.state.error.cardnumberError &&
                                            <Alert
                                                variant='danger'
                                                className='card' >
                                                {props.state.error.cardnumberError}
                                            </Alert>}
                                        <p className='cardlabel'>{t('CVV')}</p>
                                        <input
                                            className='cvv'
                                            name={CVV}
                                            placeholder='Enter CVV'
                                            onChange={(e) => props.change(e)} />
                                        {props.state.error.cvvError &&
                                            <Alert
                                                variant='danger'
                                                className='card' >
                                                {props.state.error.cvvError}
                                            </Alert>
                                        }
                                    </div>}
                                <p>
                                    <input
                                        type='radio'
                                        name='payment'
                                        value={CASH_ON_DELIVERY}
                                        onChange={(e) => props.payment(e)} />
                                    {t('Cash On Delivery')}
                                </p>
                                <p>
                                    <strong>{t('Bill')} </strong> : Rs.
                                    {props.state.bill}/-
                                </p>
                                <Button
                                    variant='primary'
                                    onClick={() => props.order(user, props.state.payment)}
                                    disabled={props.state.error.cardnumberError ||
                                        props.state.error.cvvError ||
                                        !props.state.disabled} >
                                    {t('Confirm Order')}
                                </Button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container >
    )
}

export default OrderComponent;