import { Container, Table } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function OrderDetailComponent(props) {
    const { order } = props.state;
    const { orderdetail } = props.state;
    const { t } = useTranslation();
    return (
        <Container >
            <Table className='orderdetail'>
                <tbody>
                    <tr>
                        <td>
                            <h2 id='productsheader'> {t('User Details')} </h2>
                        </td>
                        <td>
                            <h2 id='productsheader'> {t('Order Details')} </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className='paymentdetails'>
                                <p>
                                    <strong> {t('UserName')} </strong> :
                                    <br />
                                    {`${orderdetail.firstname} ${orderdetail.lastname}`}
                                </p>
                                <p>
                                    <strong> {t('Billing Address')} </strong>  :
                                    <br />
                                    {`${orderdetail.firstname} ${orderdetail.lastname},`}
                                    <br />
                                    {`${orderdetail.addressline1} ${orderdetail.addressline2}`}
                                    <br />
                                    {`${orderdetail.city}, `}
                                    <br />
                                    {`${orderdetail.state} - ${orderdetail.pincode}. `}
                                </p>
                                <p>
                                    <strong> {t('Mobile')} </strong> :
                                    <br />
                                    {orderdetail.mobile}
                                </p>
                            </div>
                        </td>
                        <td>
                            <div className='paymentdetails'>
                                <p>
                                    <strong> {t('Ordered On')} </strong> :
                                    <br />
                                    {order.date.slice(0, 10)}
                                </p>
                                <p>
                                    <strong> {t('Bill Amount')} </strong> :
                                    <br />
                                    Rs.{order.bill}/-
                                </p>
                                <p>
                                    <strong> {t('Payment Type')} </strong> :
                                    <br />
                                    {order.payment}
                                </p>
                                <p>
                                    <strong>{t('Order Status')} </strong> :
                                    <br />
                                    {order.status}!
                                </p>
                                {order.card.cardnumber &&
                                    <p>
                                        <strong> {t('Card Number')} </strong> :
                                        <br />
                                        xxxx xxxx xxxx {order.card.cardnumber.slice(15)}
                                    </p>
                                }
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
            <Table className='orderdetail'>
                <tbody>
                    <tr>
                        <td colSpan='2'>
                            <h2 id='productsheader'> {t('Product Details')} </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img
                                src={order.product.ProductImage}
                                alt={order.product.name} />
                        </td>
                        <td>
                            <div className='productdetails'>
                                <p>
                                    <strong> {t('Product Name')} </strong> :
                                    {order.product.name}
                                </p>
                                <p>
                                    <strong>{t('Price')} </strong> : Rs.
                                    {order.product.price}/-
                                </p>
                                <p>
                                    <strong>{t('Brand')} </strong> :
                                    {order.product.brand}
                                </p>
                                <p>
                                    <strong>{t('Description')} </strong> :
                                    {order.product.desc}.
                                </p>
                                <p>
                                    <strong>{t('Number of Items')} </strong> :
                                    {order.count}
                                </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    )
}

export default OrderDetailComponent;