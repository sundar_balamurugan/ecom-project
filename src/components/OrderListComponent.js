import { Container, Table, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function OrderListComponent(props) {
    const orders = props.state;
    const { t } = useTranslation();
    return (
        <Container>
            <Table id='orderstable'>
                <tbody>
                    <tr>
                        <td colSpan='2' >
                            <h2 id='productsheader'> {t('Your Orders')} </h2>
                        </td>
                    </tr>
                    {orders && orders.map((order, index) =>
                        <tr key={index} >
                            <td>
                                <img
                                    alt={order.product.name}
                                    src={order.product.ProductImage} />
                            </td>
                            <td>
                                <div className='productdetails' >
                                    <p>
                                        <strong>{t('Product Name')} </strong> :
                                        {order.product.name}
                                    </p>
                                    <p>
                                        <strong>{t('Brand')} </strong> :
                                        {order.product.brand}
                                    </p>
                                    <p>
                                        <strong>{t('Bill')} </strong> : Rs.
                                        {order.bill}/-
                                    </p>
                                    <p>
                                        <strong>{t('Ordered On')} </strong> :
                                        {order.date.slice(0, 10)}
                                    </p>
                                    <p>
                                        <strong>{t('Order Status')} </strong> :
                                        {order.status}!
                                    </p>
                                    <Button
                                        variant='primary'
                                        onClick={(e) => { props.view(index) }}>
                                        {t('View Details')}
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    )}
                </tbody>
            </Table>
        </Container>
    )
}

export default OrderListComponent;