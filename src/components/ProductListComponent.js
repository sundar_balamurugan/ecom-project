import {
    Container,
    Navbar,
    NavDropdown,
    Form,
    FormControl,
    Nav,
    Row,
    Col,
    Button
} from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function ProductListComponent(props) {
    const { t, i18n } = useTranslation();
    return (
        <Container>
            <h2 id='productsheader'>{t('Available Products')} </h2>
            <Navbar bg="dark" variant="dark" >
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavDropdown title={t('Brand')}
                            id="basic-nav-dropdown"
                            className="nav1">
                            <NavDropdown.Item
                                onClick={() => props.filter('Acer')}>
                                Acer</NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={() => props.filter('Dell')}>
                                Dell</NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={() => props.filter('Lenovo')}>
                                Lenovo</NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={() => props.filter('Samsung')}>
                                Samsung</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={t('Product')}
                            id="basic-nav-dropdown"
                            className="nav1">
                            <NavDropdown.Item
                                onClick={(v) => props.filter('Mobile')}>
                                Mobile</NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={(v) => props.filter('Laptop')}>
                                Laptop</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={t('Price')}
                            id="basic-nav-dropdown"
                            className="nav1">
                            <NavDropdown.Item
                                onClick={(v) => props.filter(undefined, 10000, 15000)}>
                                10000-15000</NavDropdown.Item>
                            <NavDropdown.Item
                                onClick={(v) => props.filter(undefined, 15000, 20000)}>
                                15000-20000</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>{' '}
                    <Form inline>
                        <FormControl type="text" id="search"
                            placeholder={t('Search by Brand productName')}
                            className="mr-sm-8"
                            onChange={(e) => { props.search(e.target.value) }} />
                    </Form>
                </Navbar.Collapse>
            </Navbar>
            <Row id='productstable' sm="3" >
                {props.products.map((product) =>
                    <Col key={product.id} xs="auto" md={4} lg="auto">
                        <p className='productimage'>
                            <img
                                alt={product.name}
                                src={product.ProductImage} />
                        </p>
                        <div className='productdetails'>
                            <p>
                                <strong> {t('Product Name')} </strong> :
                                {product.name}
                            </p>
                            <p>
                                <strong>{t('Price')} </strong> : Rs.
                                {product.price}/-
                            </p>
                            <p>
                                <strong>{t('Brand')} </strong> :
                                {product.brand}
                            </p>
                            <Button variant='primary'
                                onClick={() => { props.order(product) }}
                            >
                                {t('Buy now')}
                            </Button>{' '}
                            <Button variant='warning' className="detailbutton"
                                onClick={() => { props.view(product) }}
                            >
                                {t('Detail')}
                            </Button>
                        </div>
                    </Col>
                )}
            </Row>
        </Container>
    )
}

export default ProductListComponent;