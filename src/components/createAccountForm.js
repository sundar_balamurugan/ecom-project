import { Alert, Button } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import {
    FIRST_NAME,
    LAST_NAME,
    MOBILE,
    PASSWORD,
    ADDRESS_LINE_1,
    ADDRESS_LINE_2,
    CITY,
    STATE,
    PINCODE,
    CONFIRM_PASSWORD
} from '../utils/constants/inputNames';

function CreateAccountForm(props) {
    const error = props.state.error;
    const invalid = error.firstnameError || error.lastnameError ||
        error.mobileError || error.addressline1Error ||
        error.addressline2Error || error.cityError ||
        error.stateError || error.pincodeError ||
        error.passwordError || error.confirmpassError;
    const { t } = useTranslation();

    return (
        <form>
            <div id='registerForm'>
                <p id='formtitle'>{t('Account')}</p>
                <div>
                    <p className='label'>{t('Firstname')}</p>
                    <input
                        name={FIRST_NAME}
                        value={props.state.firstname}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter Firstname')} />
                    {error.firstnameError &&
                        <Alert variant='danger' className='span'>
                            {error.firstnameError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Lastname')}</p>
                    <input
                        name={LAST_NAME}
                        value={props.state.lastname}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter lastname')} />
                    {error.lastnameError &&
                        <Alert variant='danger' className='span'>
                            {error.lastnameError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Mobile')}</p>
                    <input
                        id='countrycode'
                        name='countrycode'
                        value='+91'
                        readOnly />
                    <input
                        name={MOBILE}
                        value={props.state.mobile}
                        onChange={(e) => props.change(e)}
                        id='mobileinput'
                        placeholder={t('Enter Mobile')} />
                    {error.mobileError &&
                        <Alert variant='danger' className='span'>
                            {error.mobileError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Address Line1')}</p>
                    <input
                        name={ADDRESS_LINE_1}
                        value={props.state.addressline1}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter Address')} />
                    {error.addressline1Error &&
                        <Alert variant='danger' className='span'>
                            {error.addressline1Error}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Address Line2 (optional)')}</p>
                    <input
                        name={ADDRESS_LINE_2}
                        value={props.state.addressline2}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter Address')} />
                    {error.addressline2Error &&
                        <Alert variant='danger' className='span'>
                            {error.addressline2Error}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('City')}</p>
                    <input
                        name={CITY}
                        value={props.state.city}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter City')} />
                    {error.cityError &&
                        <Alert variant='danger' className='span'>
                            {error.cityError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('State')}</p>
                    <select
                        name={STATE}
                        onChange={(e) => props.change(e)}
                        value={props.state.state}
                        className='forminput' >
                        <option value='' > {t('Select State')} </option>
                        {props.state.stateList.map((state, index) =>
                            <option key={index} value={state}>
                                {state}
                            </option>)
                        }
                    </select>
                    {error.stateError &&
                        <Alert variant='danger' className='span'>
                            {error.stateError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Pincode')}</p>
                    <input
                        name={PINCODE}
                        value={props.state.pincode}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter Pincode')} />
                    {error.pincodeError &&
                        <Alert variant='danger' className='span'>
                            {error.pincodeError}
                        </Alert>
                    }
                </div>
                <div>
                    <p className='label'>{t('Password')}</p>
                    <input
                        type='password'
                        name={PASSWORD}
                        value={props.state.password}
                        onChange={(e) => props.change(e)}
                        className='forminput'
                        placeholder={t('Enter Password')} />
                    {error.passwordError &&
                        <Alert variant='danger' className='span'>
                            {error.passwordError}
                        </Alert>
                    }
                </div>
                {!props.state.islogin &&
                    <div>
                        <p className='label'>{t('Confirm Password')}</p>
                        <input
                            type='password'
                            name={CONFIRM_PASSWORD}
                            value={props.state.confirmpass}
                            onChange={(e) => props.change(e)}
                            className='forminput'
                            placeholder={t('Re-Enter Password')} />
                        {error.confirmpassError &&
                            <Alert variant='danger' className='span'>
                                {error.confirmpassError}
                            </Alert>
                        }
                    </div>
                }
                {props.state.islogin &&
                    <Button
                        type='Submit'
                        id='formbutton'
                        onClick={(e) => props.edit(e)}>
                        {t('Edit')} </Button>
                }
                {!props.state.islogin &&
                    <Button
                        type='Submit'
                        id='formbutton'
                        onClick={(e) => props.submit(e)}
                        disabled={invalid} >
                        {t('Create')} </Button>
                }
            </div>
        </form>
    );
}

export default CreateAccountForm;