import React from 'react';
import { getCartProducts, RemovecartProduct } from '../actions/productsActions';
import { changeUser } from '../actions/userActions'
import { Container, Table, Button } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { cart } from '../utils/constants/initialData';
import { withTranslation } from 'react-i18next';

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = cart;
    }

    componentDidMount() {
        this.props.changeUser(JSON.parse(window.localStorage.getItem('id')))
        this.props.getCartProducts();
    }

    handleRemove = (product) => {
        const id = product.id;
        this.props.RemovecartProduct(id, product);
        this.props.getCartProducts()
    }

    render() {
        const lng= this.props.match.params.lng;
        if(lng){
             localStorage.setItem('language',lng);
        }
        const { cartproduct } = this.props;
        const { t } = this.props;
        let bill = 0;
        this.props.cartproduct && this.props.cartproduct.map((product) => {
            return bill += Number(product.price)
        })

        return (
            < Container >
                <Table id='productstable'>
                    <tbody>
                        <tr>
                            <td colSpan='2'>
                                <h2 id='productsheader'> {t('Your Cart')} </h2>

                            </td>
                        </tr>
                        {cartproduct && cartproduct.map((product) =>
                            <tr key={product.id} >
                                <td>
                                    <img
                                        alt={product.name}
                                        src={product.ProductImage} />
                                </td>
                                <td>
                                    <div className='productdetails'>
                                        <p>
                                            <strong> {t('Product Name')} </strong> :
                                            {product.name}
                                        </p>
                                        <p> <strong>{t('Price')} </strong> : Rs.
                                            {product.price}/-
                                        </p>
                                        <p> <strong>{t('Brand')} </strong> :
                                            {product.brand}
                                        </p>
                                        <Button
                                            variant='primary'
                                            onClick={() => { this.handleRemove(product) }}
                                        >
                                            {t('Remove')}
                                        </Button>
                                    </div>
                                </td>
                            </tr>
                        )}
                        {cartproduct && cartproduct.length === 0 ?
                            <tr>
                                <td colSpan='2' >
                                    <h2 id='productsheader'>
                                        {t('You Have No Products in Cart')}
                                    </h2>
                                </td>
                            </tr> :
                            <tr>
                                <td>
                                    <h2 id='productsheader'>
                                        {t('Total Bill')} : RS. {bill} /-
                                    </h2>
                                </td>
                                <td>
                                    <h3 id='productsheader'>
                                        <Button className='buy-now' >
                                            {t('Buy now')}
                                        </Button>
                                    </h3>
                                </td>
                            </tr>
                        }
                    </tbody>
                </Table>
            </Container >
        )
    }
}

function mapStateToProps(state) {
    return {
        cartproduct: state.cartproduct,
        loguser: state.loggeduser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getCartProducts, RemovecartProduct, changeUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Cart));