import React from 'react';
import { connect } from 'react-redux';
import { createUser } from '../actions/userActions';
import { createAccount } from '../utils/constants/initialData';
import { validate } from '../utils/validation';
import { bindActionCreators } from 'redux';
import CreateAccountForm from '../components/CreateAccountForm';

class CreateAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = createAccount;
    }

    handleChange = ({ target } = {}) => {
        if (target) {
            const { name, value } = target;
            let change;
            if (name === 'confirmpass') {
                const exp = new RegExp('^' + this.state.password + '$');
                change = validate(name, value, this.state.error, exp);
            } else {
                change = validate(name, value, this.state.error);
            }
            this.setState({ [name]: value, error: change.error });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createUser(this.state);
        this.props.fetchUser();
        this.props.history.push(`/${this.props.match.params.lng}/login`);
    }

    handledit = (e) => {
        e.preventDefault();
        this.props.createUser(this.state);
        this.props.history.push(`/${this.props.match.params.lng}/`);
    }

    componentDidMount = () => {
        if (this.props.logUser) {
            this.setState({
                firstname: this.props.logUser.firstname,
                lastname: this.props.logUser.lastname,
                mobile: this.props.logUser.mobile,
                addressline1: this.props.logUser.addressline1,
                addressline2: this.props.logUser.addressline2,
                city: this.props.logUser.city,
                state: this.props.logUser.state,
                pincode: this.props.logUser.pincode,
                password: this.props.logUser.password,
                confirmpass: this.props.logUser.confirmpass,
                islogin: true
            })
        }
    }

    render() {
        const lng = this.props.match.params.lng;
        if (lng) {
            localStorage.setItem('language', lng);
        }
        return (
            <CreateAccountForm
                state={this.state}
                change={this.handleChange}
                submit={this.handleSubmit}
                edit={this.handledit}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        logUser: state.loggedUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ createUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccount);