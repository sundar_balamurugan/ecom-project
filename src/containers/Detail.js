import React from 'react';
import { detail } from '../utils/constants/initialData';
import DetailComponent from '../components/DetailComponent';
import Loader from '../utils/images/loader.gif';
import axios from 'axios';
import History from '../routers/History';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { cartProduct } from '../actions/productsActions';

class Detail extends React.Component {
    constructor(props) {
        super(props);
        this.state = detail;
    }

    componentDidMount() {
        const id = this.props.match.params.id
        axios.get(`https://60cad89c21337e0017e43279.mockapi.io/products/${id}`)
            .then((res) => {
                this.setState({ product: res.data })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    handlecart = (product) => {
        if (!this.props.logUser) {
            History.push(`/${this.props.match.params.lng}/login`)
        }
        else {
            this.props.cartProduct(product);
            this.setState({ cartsuccess: true })
        }
    }

    render() {
        if (!this.state.product) {
            return (
                <div id='loader'>
                    <img src={Loader} alt='Loader' />
                </div>)
        }
        const lng = this.props.match.params.lng;
        console.log(lng)
        if (lng) {
            localStorage.setItem('language', lng);
        }
        return (
            <DetailComponent
                state={this.state}
                cart={this.handlecart}
            />
        )
    }
}

function mapStateToProps(state) {
    return { logUser: state.loggedUser }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ cartProduct }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

