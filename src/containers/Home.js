import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Container } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import History from '../routers/History';
import { getAllProducts } from '../actions/productsActions';
import Loader from '../utils/images/loader.gif';
import { myHoc } from '../components/HomeWrapped';
import Hoc from '../components/Hoc';
import { withTranslation } from 'react-i18next';
import Banner from '../components/Banner';

const BtnColorContext = React.createContext('btn btn-darkyellow')

const HigherOrder = myHoc(Hoc);

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Lets do'
    };
  }

  componentDidMount() {
    getAllProducts()
      .then((res) => {
        this.setState({
          products: res.data
        })
      })
      .catch((err) => {
        console.log(err);
      })
  }

  handleShopping = (v) => {
    const id = this.props.match.params.id;
    History.push(`/${this.props.match.params.lng}/products`)
  }

  handleproduct = (product) => {
    History.push(`/${this.props.match.params.lng}/detail/${product.id}`)
  }
  static contextType = BtnColorContext;
  render() {
    const products = this.state.products &&
      this.state.products.filter((product, id) => id < 4)

    if (!this.state.products) {
      return (
        <div id='loader'>
          <img src={Loader} alt='Loader' />
        </div>
      )
    }

    const { t, i18n } = this.props;
    const lng = this.props.match.params.lng;
    console.log(lng)
    if (lng) {
      localStorage.setItem('language', lng);
    }

    return (
      <Container className="banner">
        <Banner />
        <Button variant='info' className={this.context}
          id="shoppingButton"
          onClick={(v) => { this.handleShopping(v) }}>
          {t('Go for shopping')}
        </Button>
        <h3 id="formaltext">{t('Frequently Visited Products..')}</h3>
        <Container>
          <Row id="productstable" xs="auto" md={4} lg="auto">
            {products.map((product) =>
              <Col sm="true" key={product.id} onClick={() => this.handleproduct(product)}>
                <p className='productimage'>
                  <img
                    alt={product.name}
                    src={product.ProductImage} />
                </p>
                <div className='productdetails'>
                  <p>
                    <strong>{product.brand} {product.name} </strong>
                  </p>
                </div>
              </Col>
            )}
          </Row>
        </Container>
      </Container>
    )
  }
}

export default withTranslation()(Home);
