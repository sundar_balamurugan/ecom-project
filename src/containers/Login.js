import React from 'react';
import { login } from '../utils/constants/initialData';
import { connect } from 'react-redux';
import { fetchUser } from '../actions/userActions';
import LoginForm from '../components/LoginForm';
import { validate } from '../utils/validation';
import { bindActionCreators } from 'redux';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = login;
    }

    handleChange = ({ target } = {}) => {
        if (target) {
            const { name, value } = target;
            const change = validate(name, value, this.state.error);
            this.setState({ [name]: value, error: change.error });
        }
    }

    handleLogin = (v) => {
        v.preventDefault();
        const user = {
            mobile: this.state.mobile,
            password: this.state.password
        };
        this.props.fetchUser(user);
        window.localStorage.setItem('reqLogin', true);
    }

    componentDidUpdate() {
        if (JSON.parse(window.localStorage.getItem('reqLogin'))) {
            if (this.props.location.state) {
                this.setState({ loginError: true });
                window.localStorage.setItem('reqLogin', false);
            }
        }
    }

    render() {
        const lng= this.props.match.params.lng;
        if(lng){
             localStorage.setItem('language',lng);
        }
        return (
            <LoginForm
                state={this.state}
                change={this.handleChange}
                submit={this.handleLogin}
            />
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchUser }, dispatch);
}

export default connect(null, mapDispatchToProps)(Login);