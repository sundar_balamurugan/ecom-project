import React from 'react';
import { connect } from 'react-redux';
import { changeUser } from '../actions/userActions';
import { addOrder } from '../actions/productsActions';
import { order } from '../utils/constants/initialData';
import { bindActionCreators } from 'redux';
import OrderComponent from '../components/OrderComponent';
import { CARD_NUMBER } from '../utils/constants/inputNames';
import { validate } from '../utils/validation';
import { paymentType } from '../utils/payments';
import History from '../routers/History';

class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = order;
    }

    componentDidMount() {
        const { products } = this.props;
        this.props.changeUser(JSON.parse(window.localStorage.getItem('id')))
        this.setState({ products })
    }

    handlePayment = ({ target } = {}) => {
        if (target) {
            const result = paymentType(target.value, order);
            this.setState({
                payment: target.value,
                debitCardDetail: result.debitCardDetail,
                creditCardDetail: result.creditCardDetail,
                cardNumber: result.cardNumber,
                cvv: result.cvv,
                error: result.error
            });
        }
    }

    handleChange = ({ target } = {}) => {
        if (target) {
            if (target.name === CARD_NUMBER) {
                const length = target.value.length;
                if (length === 4 || length === 9 || length === 14) {
                    target.value += ' ';
                }
            }
            const { name, value } = target;
            const change = validate(name, value, this.state.error);
            this.setState({ [name]: value, error: change.error });
        }
    }

    handlecount = (type, product) => {
        let count = this.state.count;
        let value = count;
        let price = this.state.bill;
        if (value !== 0) {
            value = type === 'increment' ? value + 1 : value - 1;
            price = type === 'increment' ?
                price + Number(product.price) :
                price - Number(product.price);
        }

        else {
            value = type === 'increment' ? value + 1 : value;
            price = type === 'increment' ? price + Number(product.price) : price;
        }
        count = value;
        this.setState({ count, bill: price });
    }

    handleorder = (user, payment) => {
        const card = { cardnumber: this.state.cardnumber, cvv: this.state.cvv };
        const orders = this.state.products.map((product) => {
            return {
                product,
                payment,
                date: new Date(),
                card,
                status: 'Placed',
                count: this.state.count,
                bill: product.price * this.state.count
            };
        });
        this.props.addOrder(user, orders);
        this.setState({ success: true })
    }

    handleRedirect = (path) => {
        History.push(`/${this.props.match.params.lng}/`)
    }

    render() {
        if (!this.state.products) {
            return <h1>Loading..</h1>
        }
        const lng = this.props.match.params.lng;
        if (lng) {
            localStorage.setItem('language', lng);
        }
        return (
            <div>
                <OrderComponent
                    state={this.state}
                    user={this.props.logUser}
                    change={this.handleChange}
                    count={this.handlecount}
                    payment={this.handlePayment}
                    order={this.handleorder}
                    redirect={this.handleRedirect}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        products: state.products,
        logUser: state.loggedUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ changeUser, addOrder }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);