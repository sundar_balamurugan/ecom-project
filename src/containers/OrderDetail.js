import React from 'react';
import axios from 'axios';
import OrderDetailComponent from '../components/OrderDetailComponent';

class OrderDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: []
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        const productindex = this.props.location.state.state;
        axios.get(`https://60cad89c21337e0017e43279.mockapi.io/user/${id}`)
            .then((res) => {
                const orderdetail = res.data;
                const order = res.data.orders[productindex];
                this.setState({ order, orderdetail });
            })
            .catch((err) => {
                console.log(err);
            })
    }

    render() {
        if (!this.state.orderdetail || !this.state.order) {
            return <h1>Loading</h1>
        }
        const lng= this.props.match.params.lng;
        if(lng){
             localStorage.setItem('language',lng);
        }
        return (
            <div>
                <OrderDetailComponent
                    state={this.state}
                />
            </div>
        )
    }
}

export default OrderDetail;