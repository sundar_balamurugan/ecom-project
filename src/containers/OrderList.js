import React from 'react';
import OrderListComponent from '../components/OrderListComponent';
import axios from 'axios';
import History from '../routers/History';
import { orderList } from '../utils/constants/initialData';
import ReactPaginate from 'react-paginate';
import { Container } from 'react-bootstrap';

class OrderList extends React.Component {
    constructor(props) {
        super(props);
        this.state = orderList;
    }

    fetchData = () => {
        const count = Math.ceil(this.state.order.orders.length / this.state.perPage);
        const pageData = this.state.order.orders.slice(this.state.offset,
            this.state.offset + this.state.perPage);
        this.setState({ pageData, pageCount: count });
    }

    handlePageChange = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;
        this.setState({ currentPage: selectedPage, offset },
            () => this.fetchData());
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`https://60cad89c21337e0017e43279.mockapi.io/user/${id}`)
            .then((res) => {
                this.setState({ order: res.data }, () => this.fetchData())
            })
            .catch((err) => {
                console.log(err);
            })
    }

    handleView = (index) => {
        const productindex = index;
        const id = this.state.order.id;
        History.push(`/order-detail/${id}/${this.props.match.params.lng}`, { state: productindex });
    }

    render() {
        const lng= this.props.match.params.lng;
        if(lng){
             localStorage.setItem('language',lng);
        }
        return (
            <div>
                <OrderListComponent
                    state={this.state.pageData}
                    view={this.handleView}
                />

                <Container>
                    <div id='footer' >
                        <ReactPaginate
                            previousLabel={"prev"}
                            nextLabel={"next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={this.state.pageCount}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={3}
                            onPageChange={this.handlePageChange}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"}
                        />
                    </div>
                </Container>
            </div>
        )
    }
}


export default OrderList;