import React from 'react';
import { getAllProducts } from '../actions/productsActions';
import { product } from '../utils/constants/initialData';
import ProductListComponent from '../components/ProductListComponent';
import Loader from '../utils/images/loader.gif';
import History from '../routers/History';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getProducts } from '../actions/productsActions';

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = product;
        this.initialdata = undefined;
    }

    componentDidMount() {
        getAllProducts()
            .then((res) => {
                this.setState({
                    products: res.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
        const timer = setTimeout(() => {
            if (this.props.location.state) {
                this.handlefilter(this.props.location.state.state)
            }
        }, 1000);
    }

    handlefilter = (data, min, max) => {
        let products;
        if (data || this.initialdata) {
            this.initialdata = data || this.initialdata;
            products = this.state.products.filter((product) => {
                return product.brand === this.initialdata ||
                    product.name === this.initialdata ||
                    product.category === this.initialdata
            })
        }
        if (min || max) {
            if (this.initialdata) {
                products = products.filter((product) =>
                    product.price >= min && product.price <= max)
            }
            else {
                products = this.state.products.filter((product) =>
                    product.price >= min && product.price <= max)
            }
        }
        this.setState({
            filteredproducts: products,
            isfilter: true
        })

    }

    handlesearch = (search) => {
        let products;
        products = this.state.products.filter((product) => {
            return product.brand.toLowerCase().includes(search.toLowerCase()) ||
                product.name.toLowerCase().includes(search.toLowerCase())
        })
        this.setState({
            filteredproducts: products,
            isfilter: true
        })

    }

    handleview = (product) => {
        History.push(`/${this.props.match.params.lng}/detail/${product.id}`)
    }

    handleorder = (product) => {
        this.props.getProducts([product]);
        History.push(`/${this.props.match.params.lng}/order`)
    }

    render() {
        if (this.state.isfilter) {
            this.state.products = this.state.filteredproducts;
        }
        const lng = this.props.match.params.lng;
        console.log(lng)
        if (lng) {
            localStorage.setItem('language', lng);
        }
        return (
            <div>
                {this.state.products.length === 0 ?
                    (<div id='loader'>
                        <img src={Loader} alt='Loader' />
                    </div>)
                    :
                    (<>
                        <ProductListComponent
                            products={this.state.products}
                            filter={this.handlefilter}
                            search={this.handlesearch}
                            view={this.handleview}
                            order={this.handleorder}
                        />
                    </>)
                }
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getProducts }, dispatch);
}

export default connect(null, mapDispatchToProps)(ProductList);