import { CHANGE_USER, GET_PRODUCTS, LOG_OUT, FETCH_CARTPRODUCT, CREATE_USER } from "../actions/actionTypes";

const initialstate = {
    user: {
        "firstname": "sundar",
        "lastname": "bala",
        "mobile": "8489876537",
        "password": "radnus1234",
        "addressline1": "thendral nagar anakaputhur",
        "addressline2": "",
        "state": "Tamil Nadu",
        "city": "chennai",
        "pincode": "123456",
        "orders": [],
        "cart": []
    },

}

const userReducer = (state = initialstate, action) => {
    switch (action.type) {

        case CREATE_USER:
            return {
                ...state,
                user: action.user
            };

        case CHANGE_USER:
            window.localStorage.setItem('isLogin', true);
            return {
                ...state,
                loggedUser: action.user
            };

        case LOG_OUT:
            window.localStorage.setItem('isLogin', false);
            window.localStorage.removeItem('id')
            return {
                ...state,
                loggedUser: undefined
            };

        case FETCH_CARTPRODUCT:
            return {
                state,
                cartproduct: action.payload
            }

        case GET_PRODUCTS:
            return {
                ...state,
                products: action.products
            };

        default: return state;
    }
}

export default userReducer;
