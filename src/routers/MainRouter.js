import React from 'react';
import { Route, Switch, Router } from "react-router-dom";
import History from './History';
import Login from '../containers/Login';
import CreateAccount from '../containers/CreateAccount';
import Home from '../containers/Home';
import Navigation from '../components/Navigation';
import ProductList from '../containers/ProductList';
import Detail from '../containers/Detail';
import PrivateRoute from './PrivateRoute';
import Cart from '../containers/Cart';
import Order from '../containers/Order';
import OrderList from '../containers/OrderList';
import OrderDetail from '../containers/OrderDetail';
import { BASE_URL } from '../utils/constants/inputNames';
import SampleForm from '../Form/SampleForm';

function MainRouter(props) {
    return (
        <Router history={History}>
            <Navigation />
            <Switch>
                <Route exact path={BASE_URL + '/'} component={Home}></Route>
                <Route exact path={BASE_URL + '/login'} component={Login}></Route>
                <Route path={BASE_URL + '/create-account'} component={CreateAccount}></Route>
                <PrivateRoute path={BASE_URL + '/my-account'} component={CreateAccount} />
                <Route path={BASE_URL + '/products'} component={ProductList}></Route>
                <Route path={BASE_URL + '/detail/:id'} component={Detail} />
                <PrivateRoute path={BASE_URL + '/cart'} component={Cart} />
                <PrivateRoute path={BASE_URL + '/order'} component={Order} />
                <PrivateRoute path={BASE_URL + '/your-order/:id'} component={OrderList} />
                <PrivateRoute path={BASE_URL + '/order-detail/:id/'} component={OrderDetail} />
                <Route exact path={BASE_URL + '/form'} component={SampleForm}></Route>
            </Switch>
        </Router>
    )
}

export default MainRouter;