import { Route, Redirect } from 'react-router-dom';

function PrivateRoute( props ) {
    const { component: Component, ...rest } = props;
    const loginFlag = JSON.parse( window.localStorage.getItem( 'isLogin' ) );
    
    return (
        <Route { ...rest } render = { ( props ) => loginFlag ?
            <Component { ...props } /> :
            <Redirect to = '/login'/> }
        />
    );
}

export default PrivateRoute;