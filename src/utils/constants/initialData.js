export const login = {
    mobile: '',
    password: '',
    error: { mobileError: ' ', passwordError: ' ' }
}

export const createAccount = {
    firstname: '',
    lastname: '',
    mobile: '',
    addressline1: '',
    addressline2: '',
    city: '',
    state: '',
    pincode: '',
    password: '',
    confirmpass: '',
    error: {
        firstnameError: ' ',
        lastnameError: ' ',
        mobileError: ' ',
        passwordError: ' ',
        addressline1Error: ' ',
        addressline2Error: '',
        cityError: ' ',
        stateError: ' ',
        pincodeError: ' ',
        confirmpassError: ' ',
    },
    stateList: ['Tamil Nadu', 'Andhra Pradesh', 'Assam', 'Arunachal Pradesh', 'Bihar'],
    islogin: false
};

export const product = {
    products: [],
    filteredproducts: [],
    isfilter: false
}

export const detail = {
    product: undefined
}

export const cart = {
    bill: 0,
};

export const order = {
    products: undefined,
    success: false,
    payment: '',
    cardnumber: undefined,
    count: 0,
    cvv: '',
    creditCardDetail: false,
    debitCardDetail: false,
    error: { cardnumberError: ' ', cvvError: ' ' },
    bill: 0,
    disabled: true
}

export const orderList = {
    offset: 0,
    perPage: 1,
    currentPage: 0,
    order: []
};