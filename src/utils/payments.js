import { CASH_ON_DELIVERY, CREDIT_CARD, DEBIT_CARD } from "./constants/inputNames";

export function paymentType(value, data) {
    let { debitCardDetail, creditCardDetail, error, cardNumber, cvv } = data;
    switch (value) {
        case CREDIT_CARD:
            creditCardDetail = true;
            break;
        case DEBIT_CARD:
            debitCardDetail = true;
            break;
        case CASH_ON_DELIVERY:
            error = { cardnumberError: '', cvvError: '' };
            break;
        default:
            break;
    }
    return {
        debitCardDetail,
        creditCardDetail,
        error,
        cardNumber,
        cvv
    }
};