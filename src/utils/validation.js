import {
    MOBILE_REGEX,
    PASSWORD_REGEX,
    AlPHABETS_ONLY_REGEX,
    ADDRESS_REGEX,
    PINCODE_REGEX,
    CARDNUMBER_REGEX,
    CVV_REGEX,
    ACCOUNT_NUMBER_REGEX
} from '../utils/constants/regex';

import {
    MOBILE,
    PASSWORD,
    FIRST_NAME,
    LAST_NAME,
    ADDRESS_LINE_1,
    ADDRESS_LINE_2,
    CITY,
    STATE,
    PINCODE,
    CONFIRM_PASSWORD,
    CARD_NUMBER,
    CVV,
    ACCOUNT_NUMBER
} from '../utils/constants/inputNames'

export function validate(name, value, errors, pattern) {
    let result = {};
    switch (name) {
        case MOBILE:
            result = {
                error: MOBILE_REGEX.test(value)
                    ? { ...errors, mobileError: '' }
                    : { ...errors, mobileError: 'Invalid Mobile Number' }
            };
            break;
        case PASSWORD:
            result = {
                error: PASSWORD_REGEX.test(value)
                    ? { ...errors, passwordError: '' }
                    : { ...errors, passwordError: 'Password must be more than 8 characters' }
            };
            break;
        case FIRST_NAME:
            result = {
                error: AlPHABETS_ONLY_REGEX.test(value)
                    ? { ...errors, firstnameError: '' }
                    : { ...errors, firstnameError: 'Name must have Alphabets only' }
            };
            break;
        case LAST_NAME:
            result = {
                error: AlPHABETS_ONLY_REGEX.test(value)
                    ? { ...errors, lastnameError: '' }
                    : { ...errors, lastnameError: 'Name must have Alphabets only' }
            };
            break;
        case ADDRESS_LINE_1:
            result = {
                error: ADDRESS_REGEX.test(value)
                    ? { ...errors, addressline1Error: '' }
                    : { ...errors, addressline1Error: 'Invalid Address' }
            };
            break;
        case ADDRESS_LINE_2:
            result = {
                error: ADDRESS_REGEX.test(value)
                    ? { ...errors, addressline2Error: '' }
                    : { ...errors, addressline2Error: 'Invalid Address' }
            };
            break;
        case CITY:
            result = {
                error: ADDRESS_REGEX.test(value)
                    ? { ...errors, cityError: '' }
                    : { ...errors, cityError: 'Invalid City' }
            };
            break;
        case STATE:
            result = {
                error: ADDRESS_REGEX.test(value)
                    ? { ...errors, stateError: '' }
                    : { ...errors, stateError: 'Invalid State' }
            };
            break;
        case PINCODE:
            result = {
                error: PINCODE_REGEX.test(value)
                    ? { ...errors, pincodeError: '' }
                    : { ...errors, pincodeError: 'Invalid Pincode' }
            };
            break;
        case CONFIRM_PASSWORD:
            result = {
                error: pattern.test(value)
                    ? { ...errors, confirmpassError: '' }
                    : { ...errors, confirmpassError: 'Password Does Not Match' }
            };
            break;

        case CARD_NUMBER:
            result = {
                error: CARDNUMBER_REGEX.test(value)
                    ? { ...errors, cardnumberError: '' }
                    : { ...errors, cardnumberError: 'Invalid CardNumber' }
            };
            break;
        case CVV:
            result = {
                error: CVV_REGEX.test(value)
                    ? { ...errors, cvvError: '' }
                    : { ...errors, cvvError: 'Invalid CVV' }
            };
            break;

        case ACCOUNT_NUMBER:
            result = {
                error: ACCOUNT_NUMBER_REGEX.test(value)
                    ? { ...errors, accNumberError: '' }
                    : { ...errors, accNumberError: 'Invalid ACCOUNT NUMBER' }
            };
            break;

        default:
            console.log('This function call have nothing to do!');
    }
    return result;
}